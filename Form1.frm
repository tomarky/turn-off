VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   0  'なし
   Caption         =   "TurnOff"
   ClientHeight    =   3270
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   3855
   Icon            =   "Form1.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3270
   ScaleWidth      =   3855
   StartUpPosition =   2  '画面の中央
   Begin VB.Timer Timer1 
      Left            =   3480
      Top             =   3000
   End
   Begin VB.CommandButton Command3 
      Caption         =   "end"
      Height          =   495
      Left            =   3240
      TabIndex        =   9
      Top             =   1680
      Width           =   495
   End
   Begin VB.CommandButton Command2 
      Caption         =   "start"
      Height          =   495
      Left            =   3240
      TabIndex        =   8
      Top             =   840
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "／"
      Height          =   495
      Index           =   7
      Left            =   3240
      TabIndex        =   7
      Top             =   120
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "｜"
      Height          =   495
      Index           =   6
      Left            =   2520
      TabIndex        =   6
      Top             =   120
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "｜"
      Height          =   495
      Index           =   5
      Left            =   1680
      TabIndex        =   5
      Top             =   120
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "｜"
      Height          =   495
      Index           =   4
      Left            =   840
      TabIndex        =   4
      Top             =   120
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "＼"
      Height          =   495
      Index           =   3
      Left            =   120
      TabIndex        =   3
      Top             =   120
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "―"
      Height          =   495
      Index           =   2
      Left            =   120
      TabIndex        =   2
      Top             =   840
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "―"
      Height          =   495
      Index           =   1
      Left            =   120
      TabIndex        =   1
      Top             =   1680
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "―"
      Height          =   495
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   2520
      Width           =   495
   End
   Begin VB.Label Label2 
      Alignment       =   2  '中央揃え
      BackColor       =   &H00404040&
      Caption         =   "TURN OFF"
      BeginProperty Font 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   21.75
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0000FFFF&
      Height          =   495
      Left            =   720
      TabIndex        =   11
      Top             =   1680
      Width           =   2415
   End
   Begin VB.Label Label1 
      Alignment       =   2  '中央揃え
      BorderStyle     =   1  '実線
      BeginProperty Font 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   20.25
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3240
      TabIndex        =   10
      Top             =   2520
      Width           =   495
   End
   Begin VB.Line Line1 
      BorderColor     =   &H8000000F&
      BorderWidth     =   5
      Index           =   3
      X1              =   0
      X2              =   3840
      Y1              =   3240
      Y2              =   3240
   End
   Begin VB.Line Line1 
      BorderColor     =   &H8000000F&
      BorderWidth     =   5
      Index           =   2
      X1              =   0
      X2              =   3840
      Y1              =   0
      Y2              =   0
   End
   Begin VB.Line Line1 
      BorderColor     =   &H8000000F&
      BorderWidth     =   5
      Index           =   1
      X1              =   3840
      X2              =   3840
      Y1              =   0
      Y2              =   3240
   End
   Begin VB.Line Line1 
      BorderColor     =   &H8000000F&
      BorderWidth     =   5
      Index           =   0
      X1              =   0
      X2              =   0
      Y1              =   0
      Y2              =   3240
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  '不透明
      Height          =   255
      Index           =   8
      Left            =   2640
      Shape           =   3  '円
      Top             =   2640
      Width           =   255
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  '不透明
      Height          =   255
      Index           =   7
      Left            =   1800
      Shape           =   3  '円
      Top             =   2640
      Width           =   255
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  '不透明
      Height          =   255
      Index           =   6
      Left            =   960
      Shape           =   3  '円
      Top             =   2640
      Width           =   255
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  '不透明
      Height          =   255
      Index           =   5
      Left            =   2640
      Shape           =   3  '円
      Top             =   1800
      Width           =   255
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  '不透明
      Height          =   255
      Index           =   4
      Left            =   1800
      Shape           =   3  '円
      Top             =   1800
      Width           =   255
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  '不透明
      Height          =   255
      Index           =   3
      Left            =   960
      Shape           =   3  '円
      Top             =   1800
      Width           =   255
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  '不透明
      Height          =   255
      Index           =   2
      Left            =   2640
      Shape           =   3  '円
      Top             =   960
      Width           =   255
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  '不透明
      Height          =   255
      Index           =   1
      Left            =   1800
      Shape           =   3  '円
      Top             =   960
      Width           =   255
   End
   Begin VB.Shape Shape3 
      BackStyle       =   1  '不透明
      Height          =   255
      Index           =   0
      Left            =   960
      Shape           =   3  '円
      Top             =   960
      Width           =   255
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  '不透明
      FillColor       =   &H80000005&
      Height          =   495
      Index           =   8
      Left            =   2520
      Shape           =   3  '円
      Top             =   2520
      Width           =   495
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  '不透明
      FillColor       =   &H80000005&
      Height          =   495
      Index           =   7
      Left            =   1680
      Shape           =   3  '円
      Top             =   2520
      Width           =   495
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  '不透明
      FillColor       =   &H80000005&
      Height          =   495
      Index           =   6
      Left            =   840
      Shape           =   3  '円
      Top             =   2520
      Width           =   495
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  '不透明
      FillColor       =   &H80000005&
      Height          =   495
      Index           =   5
      Left            =   2520
      Shape           =   3  '円
      Top             =   1680
      Width           =   495
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  '不透明
      FillColor       =   &H80000005&
      Height          =   495
      Index           =   4
      Left            =   1680
      Shape           =   3  '円
      Top             =   1680
      Width           =   495
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  '不透明
      FillColor       =   &H80000005&
      Height          =   495
      Index           =   3
      Left            =   840
      Shape           =   3  '円
      Top             =   1680
      Width           =   495
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  '不透明
      FillColor       =   &H80000005&
      Height          =   495
      Index           =   2
      Left            =   2520
      Shape           =   3  '円
      Top             =   840
      Width           =   495
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  '不透明
      FillColor       =   &H80000005&
      Height          =   495
      Index           =   1
      Left            =   1680
      Shape           =   3  '円
      Top             =   840
      Width           =   495
   End
   Begin VB.Shape Shape2 
      BackStyle       =   1  '不透明
      FillColor       =   &H80000005&
      Height          =   495
      Index           =   0
      Left            =   840
      Shape           =   3  '円
      Top             =   840
      Width           =   495
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  '不透明
      Height          =   735
      Index           =   8
      Left            =   2400
      Shape           =   3  '円
      Top             =   2400
      Width           =   735
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  '不透明
      Height          =   735
      Index           =   7
      Left            =   1560
      Shape           =   3  '円
      Top             =   2400
      Width           =   735
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  '不透明
      Height          =   735
      Index           =   6
      Left            =   720
      Shape           =   3  '円
      Top             =   2400
      Width           =   735
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  '不透明
      Height          =   735
      Index           =   5
      Left            =   2400
      Shape           =   3  '円
      Top             =   1560
      Width           =   735
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  '不透明
      Height          =   735
      Index           =   4
      Left            =   1560
      Shape           =   3  '円
      Top             =   1560
      Width           =   735
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  '不透明
      Height          =   735
      Index           =   3
      Left            =   720
      Shape           =   3  '円
      Top             =   1560
      Width           =   735
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  '不透明
      Height          =   735
      Index           =   2
      Left            =   2400
      Shape           =   3  '円
      Top             =   720
      Width           =   735
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  '不透明
      Height          =   735
      Index           =   1
      Left            =   1560
      Shape           =   3  '円
      Top             =   720
      Width           =   735
   End
   Begin VB.Shape Shape1 
      BackStyle       =   1  '不透明
      Height          =   735
      Index           =   0
      Left            =   720
      Shape           =   3  '円
      Top             =   720
      Width           =   735
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Light(8) As Integer
Dim LightColor(23) As Long
Dim NowStage As Integer
Dim StageNextCall As Boolean
Dim StageData(19) As String
Dim StageDataOverTwenty As String

Private Sub Init()
    Dim a%, b%, i%
    For i = 0 To 5
        a = i * 10
        b = &HA0 + i * &H10
        LightColor(i * 4) = RGB(a, a, a)
        LightColor(i * 4 + 1) = RGB(0, 0, b)
        LightColor(i * 4 + 2) = RGB(b, b, 0)
        LightColor(i * 4 + 3) = RGB(b, 0, 0)
    Next i
    StageData(0) = "123123123":  StageData(1) = "020202020"
    StageData(2) = "333303333":  StageData(3) = "000020000"
    StageData(4) = "010111010":  StageData(5) = "101020101"
    StageData(6) = "030313030":  StageData(7) = "323202323"
    StageData(8) = "010000010":  StageData(9) = "300011300"
    StageData(10) = "112132112": StageData(11) = "311110101"
    StageData(12) = "313121202": StageData(13) = "230301012"
    StageData(14) = "110111011": StageData(15) = "201000320"
    StageData(16) = "220012002": StageData(17) = "102203102"
    StageData(18) = "303303010": StageData(19) = "201030102"
    NowStage = 0
    StageNextCall = True
End Sub

Private Sub LightChange(i As Integer, j As Integer)
    Light(i + j) = Light(i + j) + 1 + 4 * (Light(i + j) = 3)
End Sub

Private Sub LightChangeCall(Index As Integer)
    Dim i%, j%
    Select Case Index
    Case 0, 1, 2
        i = 6 - Index * 3
        For j = 0 To 2
            LightChange i, j
        Next j
    Case 3, 7
        i = Index \ 3
        For j = 0 To 8 Step 4
            LightChange 0, j / i - i * (i = 2)
        Next j
    Case 4, 5, 6
        i = Index - 4
        For j = 0 To 6 Step 3
            LightChange i, j
        Next j
    End Select
End Sub

Private Sub LightCheck()
    Dim c%, i%
    c = 0
    For i = 0 To 8
        c = c + Light(i)
    Next i
    If c = 0 Then
        For i = 0 To 7
            Command1(i).Enabled = False
        Next i
        MsgBox "Stage Clear !", , "Turn Off"
        NowStage = NowStage + 1
        If NowStage > 19 Then StageNextCall = True
        Command2.Caption = "next"
        Timer1.Enabled = True
        Label2.Caption = "push [next]"
        Label2.Visible = True
    End If
End Sub

Private Sub StageLoad()
    Dim c%, i%, j%
    Label1.Caption = NowStage + 1
    If NowStage < 20 Then
        For i = 0 To 8
            Light(i) = Int(Val(Mid$(StageData(NowStage), i + 1, 1)))
        Next i
    Else
        If StageNextCall Then
            Randomize Timer
            c = 0
            Do
                For i = 0 To 19
                    j = Int(Rnd * 8)
                    LightChangeCall j
                Next i
                For i = 0 To 8
                    c = c + Light(i)
                Next i
            Loop While c = 0
            StageDataOverTwenty = ""
            For i = 0 To 8
                StageDataOverTwenty = StageDataOverTwenty + Trim$(Str$(Light(i)))
            Next i
            StageNextCall = False
        Else
            For i = 0 To 8
                Light(i) = Int(Val(Mid$(StageDataOverTwenty, i + 1, 1)))
            Next i
        End If
    End If
End Sub

Private Sub TurnOnLights()
    Dim i%
    For i = 0 To 8
        Shape1(i).BorderColor = LightColor(Light(i))
        Shape1(i).BackColor = LightColor(Light(i) + 4)
        Shape2(i).BorderColor = LightColor(Light(i) + 8)
        Shape2(i).BackColor = LightColor(Light(i) + 12)
        Shape3(i).BorderColor = LightColor(Light(i) + 16)
        Shape3(i).BackColor = LightColor(Light(i) + 20)
    Next i
End Sub

Private Sub Command1_Click(Index As Integer)
    LightChangeCall Index
    TurnOnLights
    LightCheck
End Sub

Private Sub Command2_Click()
    Dim i%
    Label2.Visible = False
    Timer1.Enabled = False
    For i = 0 To 7
        Command1(i).Enabled = True
    Next i
    StageLoad
    TurnOnLights
    Command2.Caption = "retry"
End Sub

Private Sub Command3_Click()
    End
End Sub

Private Sub Form_Load()
    Dim i%
    
    Randomize Timer
    
    For i = 0 To 7
        Command1(i).Enabled = False
    Next i
    Form1.BackColor = RGB(0, 0, 0)
    For i = 0 To 8
        Light(i) = 0
        Shape1(i).BorderWidth = 4
        Shape2(i).BorderWidth = 4
        Shape3(i).BorderWidth = 4
    Next i
    Timer1.Interval = 1000
    
    Init
    TurnOnLights

    Timer1.Enabled = True
End Sub

Private Sub Label2_Click()
    Dim a$, b$, c$, d$, e$, f$, g$, h$, n$
    a = "ルール説明"
    b = "[start]を押すと９個の内のいくつかの電球に明かりが灯ります"
    c = "[―][｜][／][＼]のスイッチを押すと"
    d = "各スイッチに対応する列の電球の色が"
    e = "青→黄→赤→黒(消灯)→青の順番で切り替わります"
    f = "このゲームの目的は「ＴＵＲＮ　ＯＦＦ」です"
    g = "つまりスイッチを巧みに使って"
    h = "全ての電球を黒(消灯)にすればステージクリアなのです"
    n = Chr$(13)
    MsgBox b + n + c + n + d + n + e + n + f + n + g + n + h, , a
End Sub

Private Sub Timer1_Timer()
    Dim j%
    j = Int(Rnd * 8)
    LightChangeCall j
    TurnOnLights
End Sub
